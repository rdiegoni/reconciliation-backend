using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Reconciliation.Infrastructure.Providers
{
    public class FileProviderService<T>: IProvider<T>
    {
        private readonly string _path;
        
        public FileProviderService(string path)
        {
            _path = path;
        }
        public async Task<IEnumerable<T>> GetEntities()
        {
            using var file = File.OpenText(_path);
            var serializer = new JsonSerializer();
            var entities = (IList<T>)serializer.Deserialize(file, typeof(IList<T>));
            return await Task.FromResult(entities);
        }
    }
}
