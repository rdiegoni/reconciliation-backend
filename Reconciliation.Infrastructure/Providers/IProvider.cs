using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reconciliation.Infrastructure.Providers
{
    public interface IProvider<T>
    {
        Task<IEnumerable<T>> GetEntities();
    }
}
