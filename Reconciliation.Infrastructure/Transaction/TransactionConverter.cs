using TransactionDomain = Reconciliation.Domain.Account.Entities.Transaction;

namespace Reconciliation.Infrastructure.Transaction
{
    public static class TransactionConverter
    {
        public static TransactionDomain Convert(Transaction transaction)
        {
            return new TransactionDomain(transaction.Id, transaction.AccountId, transaction.Amount, transaction.Currency, transaction.Type, transaction.Datetime);
        }
    }
}
