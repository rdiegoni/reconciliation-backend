using System;

namespace Reconciliation.Infrastructure.Transaction
{
    public class Transaction
    {
        public string Id { get; set; }

        public string AccountId { get; set; }
        
        public decimal Amount { get; set; }
        
        public string Currency { get; set; }
        
        public string Type { get; set; }
        
        public DateTimeOffset Datetime { get; set; }
    }
}
