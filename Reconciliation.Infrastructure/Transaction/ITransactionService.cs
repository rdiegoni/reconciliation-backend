using System.Collections.Generic;
using System.Threading.Tasks;
using TransactionDomain = Reconciliation.Domain.Account.Entities.Transaction;

namespace Reconciliation.Infrastructure.Transaction
{
    public interface ITransactionService
    {
        Task<IList<TransactionDomain>> GetTransactions();
    }
}
