using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reconciliation.Infrastructure.Providers;
using TransactionDomain = Reconciliation.Domain.Account.Entities.Transaction;

namespace Reconciliation.Infrastructure.Transaction
{
    public class TransactionService: ITransactionService
    {
        private readonly IProvider<Transaction> _provider;

        public TransactionService(IProvider<Transaction> provider)
        {
            _provider = provider;
        }
        
        public async Task<IList<TransactionDomain>> GetTransactions()
        {
            var transactions = await _provider.GetEntities();
            return transactions.Select(TransactionConverter.Convert).ToList();
        }
    }
}
