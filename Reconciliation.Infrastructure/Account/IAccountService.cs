using System.Collections.Generic;
using System.Threading.Tasks;
using AccountDomain = Reconciliation.Domain.Account.Account;

namespace Reconciliation.Infrastructure.Account
{
    public interface IAccountService
    {
        Task<IList<AccountDomain>> GetAccounts();
    }
}
