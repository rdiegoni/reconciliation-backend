using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reconciliation.Infrastructure.Providers;
using AccountDomain = Reconciliation.Domain.Account.Account;

namespace Reconciliation.Infrastructure.Account
{
    public class AccountService: IAccountService
    {
        private readonly IProvider<Account> _provider;

        public AccountService(IProvider<Account> provider)
        {
            _provider = provider;
        }
        
        public async Task<IList<AccountDomain>> GetAccounts()
        {
            var accounts = await _provider.GetEntities();
            return accounts.Select(AccountConverter.Convert).ToList();
        }
    }
}
