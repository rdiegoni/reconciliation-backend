namespace Reconciliation.Infrastructure.Account
{
    public class Account
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
