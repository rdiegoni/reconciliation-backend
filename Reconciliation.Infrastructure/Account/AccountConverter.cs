using AccountDomain = Reconciliation.Domain.Account.Account;

namespace Reconciliation.Infrastructure.Account
{
    public static class AccountConverter
    {
        public static AccountDomain Convert(Account account)
        {
            return new AccountDomain(account.Id, account.Name);
        }
    }
}
