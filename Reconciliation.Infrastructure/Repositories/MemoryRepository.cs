using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Reconciliation.Domain;

namespace Reconciliation.Infrastructure.Repositories
{
    public class MemoryRepository<TEntity>: IRepository<TEntity>
    {
        private readonly IDistributedCache _cache;
        private readonly string _key;
        
        public MemoryRepository(
            IDistributedCache cache,
            string key)
        {
            _cache = cache;
            _key = key;
        }
        
        public async Task<IList<TEntity>> GetEntities()
        {
            var value = await _cache.GetStringAsync(_key);
            
            var result = value != null 
                ? JsonConvert.DeserializeObject<IList<TEntity>>(value, GetSerializerSettings())
                : Array.Empty<TEntity>();
            return result;
        }

        public async Task Add(IEnumerable<TEntity> values)
        {
            var entities = values.ToList();
            await SetEntities(entities);
        }

        private async Task SetEntities(IList<TEntity> entities)
        {
            var value = JsonConvert.SerializeObject(entities, GetSerializerSettings());
            await _cache.SetStringAsync(_key, value);
        }

        private static JsonSerializerSettings GetSerializerSettings()
        {
            return new JsonSerializerSettings
            {
                Converters = new List<JsonConverter>
                {
                    new StringEnumConverter()
                }
            };
        }
    }
}
