FROM mcr.microsoft.com/dotnet/core/sdk:3.1 as builder

WORKDIR /source

COPY . .
RUN dotnet restore

RUN dotnet publish -c Release -o /app/out -r linux-x64 Reconciliation.Api/Reconciliation.Api.csproj

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 as runtime

WORKDIR /app

COPY --from=builder /app/out .
COPY --from=builder /source/data ./data

ENV ASPNETCORE_URLS http://*:5000
ENTRYPOINT ["dotnet", "Reconciliation.Api.dll"]
