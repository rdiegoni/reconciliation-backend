using System;
using System.Linq;
using Reconciliation.Domain.Account.Entities;
using Xunit;

namespace Reconciliation.Domain.Tests.Account
{
    public class AccountTests
    {
        [Fact]
        public void Account_WhenTransactionsDoNotExist_ShouldSetTheBalanceToZero()
        {
            // When
            var subject = new Domain.Account.Account("1", "My account");
            
            // Then
            Assert.Empty(subject.Balances);
        }
        
        [Fact]
        public void Account_WhenRefundedTransactionsDoExist_ShouldSetTheBalanceNegatively()
        {
            // When
            var transaction = new Transaction("1", "1", 1000, "GBP", TransactionType.Refunded.ToString(),
                DateTimeOffset.Now);
            
            var subject = new Domain.Account.Account("1", "My account");
            subject.AddTransactions(new[] { transaction });
            
            // Then
            Assert.Equal(1, subject.Balances.Count);
            Assert.Equal(-1000, subject.Balances[0].Amount);
            Assert.Equal("GBP", subject.Balances[0].Currency);
        }
        
        [Fact]
        public void Account_WhenChargebackTransactionsDoExist_ShouldSetTheBalanceNegatively()
        {
            // When
            var transaction = new Transaction("1", "1", 1000, "GBP", TransactionType.Chargeback.ToString(),
                DateTimeOffset.Now);
            
            var subject = new Domain.Account.Account("1", "My account");
            subject.AddTransactions(new[] { transaction });
            
            // Then
            Assert.Equal(1, subject.Balances.Count);
            Assert.Equal(-1000, subject.Balances[0].Amount);
            Assert.Equal("GBP", subject.Balances[0].Currency);
        }
        
        [Fact]
        public void Account_WhenSettledTransactionsDoExist_ShouldSetTheBalancePositively()
        {
            // When
            var transaction = new Transaction("1", "1", 1000, "GBP", TransactionType.Settled.ToString(),
                DateTimeOffset.Now);
            
            var subject = new Domain.Account.Account("1", "My account");
            subject.AddTransactions(new[] { transaction });
            
            // Then
            Assert.Equal(1, subject.Balances.Count);
            Assert.Equal(1000, subject.Balances[0].Amount);
            Assert.Equal("GBP", subject.Balances[0].Currency);
        }
        
        [Theory]
        [InlineData(new[] { 1000, 2000, 1700 }, -1300)]
        [InlineData(new[] { 1000, 2000, 3500 }, 500)]
        public void Account_WhenMultipleTransactionsDoExist_ShouldSetTheBalanceCorrectly(int[] amounts, decimal balance)
        {
            // When
            var transactions = amounts
                .Select((amount, index) =>
                {
                    var type = Enum.GetNames(typeof(TransactionType)).Where(x => x != "None").ToList()[index];
                    return new Transaction($"{index + 1}", "1", amount, "GBP",
                        type,
                        DateTimeOffset.Now);
                })
                .ToList();
            
            var subject = new Domain.Account.Account("1", "My account");
            subject.AddTransactions(transactions);
            
            // Then
            Assert.Equal(1, subject.Balances.Count);
            Assert.Equal(balance, subject.Balances[0].Amount);
            Assert.Equal("GBP", subject.Balances[0].Currency);
        }
    }
}
