using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Reconciliation.Api.Models.Configuration;

namespace Reconciliation.Api.Configurations
{
    [ExcludeFromCodeCoverage]
    public static class MvcExtensions
    {
        public static void ConfigureMvc(
            this IServiceCollection services,
            IHostEnvironment environment,
            ApplicationOptions applicationOptions)
        {
            services.AddResponseCompression();

            services.AddMvc()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.Converters.Add(new StringEnumConverter(new CamelCaseNamingStrategy()));
                });
            
            services.AddCors(options =>
            {
                options.AddPolicy("CORSPolicy", 
                    builder =>
                    {
                        builder
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowAnyOrigin();
                    });
            });

            
            services.AddHealthChecks();
            services.AddControllers();   
        }
    }
}
