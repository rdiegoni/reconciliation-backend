using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Reconciliation.Api.Factories;
using Reconciliation.Api.Models.Configuration;
using Reconciliation.Api.Services.Account;
using Reconciliation.Api.Services.Loading;
using Reconciliation.Domain;
using Reconciliation.Domain.Account;
using Reconciliation.Infrastructure.Repositories;

namespace Reconciliation.Api.Configurations
{
    [ExcludeFromCodeCoverage]
    internal static class Services
    {
        public static void ConfigureServiceCollection(
            this IServiceCollection services, 
            IConfiguration configuration,
            IHostEnvironment environment)
        {
            services.ConfigureOptions(configuration);
            var applicationOptions = services.GetOptions<ApplicationOptions>();

            var httpContextAccessor = new HttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor>(httpContextAccessor);
            
            services.AddDistributedMemoryCache();
            services.ConfigureMembers();
            services.ConfigureMvc(environment, applicationOptions);
        }
        
        private static void ConfigureOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ApplicationOptions>(configuration.GetSection("ApplicationOptions"));
        }

        private static void ConfigureMembers(this IServiceCollection services)
        {    
            services.AddSingleton<IFailureResponseFactory, FailureResponseFactory>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddHostedService<StartServices>();
            services.AddSingleton<IRepository<Account>>(servicesProvider => ActivatorUtilities.CreateInstance<MemoryRepository<Account>>(servicesProvider, "reconciliation-api-v1"));

            services.ConfigureInfraDependencies();
        }

        public static T GetOptions<T>(
            this IServiceCollection services) where T : class, new()
        {
            var serviceProvider = services.BuildServiceProvider();
            return serviceProvider.GetRequiredService<IOptions<T>>().Value;
        }
    }
}
