using Microsoft.Extensions.DependencyInjection;
using Reconciliation.Api.Models.Configuration;
using Reconciliation.Infrastructure.Providers;
using Reconciliation.Infrastructure.Transaction;
using Reconciliation.Infrastructure.Account;

namespace Reconciliation.Api.Configurations
{
    public static class InfraDependencies
    {
        public static void ConfigureInfraDependencies(
            this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITransactionService, TransactionService>();
            
            var applicationOptions = services.GetOptions<ApplicationOptions>();
            services.AddScoped<IProvider<Account>>(serviceProvider => ActivatorUtilities.CreateInstance<FileProviderService<Account>>(serviceProvider, applicationOptions.AccountPath));
            services.AddScoped<IProvider<Transaction>>(serviceProvider => ActivatorUtilities.CreateInstance<FileProviderService<Transaction>>(serviceProvider, applicationOptions.TransactionPath));
        }
    }
}
