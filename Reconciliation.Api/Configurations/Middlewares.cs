using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Hosting;
using Reconciliation.Api.Middlewares;

namespace Reconciliation.Api.Configurations
{
    [ExcludeFromCodeCoverage]
    internal static class Middlewares
    {
        public static void ConfigureMiddleware(
            this IApplicationBuilder app, 
            IHostEnvironment environment)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            // if (!environment.IsDevelopment())
            // {
            //     app.UseHttpsRedirection();
            // }
            
            app.UseGlobalExceptionHandler();
            
            app.UseRouting();
            app.UseCors("CORSPolicy");
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/up");
                endpoints.MapControllers();
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
