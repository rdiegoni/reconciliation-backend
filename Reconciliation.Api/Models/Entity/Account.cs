using System.Collections.Generic;

namespace Reconciliation.Api.Models.Entity
{
    public class Account
    {
        public Account()
        {
            Balances = new List<Balance>();
        }
        public string Id { get; set; }

        public string Name { get; set; }
        
        public IEnumerable<Balance> Balances { get; set; }
    }
}
