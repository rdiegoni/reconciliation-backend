using System;

namespace Reconciliation.Api.Models.Entity
{
    public class Transaction
    {
        public string Id { get; set; }

        public string AccountId { get; set; }
        
        public decimal Amount { get; set; }
        
        public string Currency { get; set; }
        
        public string Type { get; set; }
        
        public DateTimeOffset Date { get; set; }
    }
}
