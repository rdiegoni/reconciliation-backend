using System.Collections.Generic;

namespace Reconciliation.Api.Models.Entity
{
    public class AccountsResponse
    {
        public AccountsResponse()
        {
            Result = new List<Account>();
        }
        public IEnumerable<Account> Result { get; set; }   
    }
}
