namespace Reconciliation.Api.Models.Entity
{
    public class Balance
    {
        public decimal Amount { get; set; }
        
        public string Currency { get; set; }
    }
}
