using System.Net;

namespace Reconciliation.Api.Models.Exception
{
    public static class Errors
    {
        public static readonly ErrorCodes ApiInternalServerError = ErrorCodes.Create("api_internal_server_error", HttpStatusCode.InternalServerError);

        public static readonly ErrorCodes ApiParametersError = ErrorCodes.Create("api_parameters_error", HttpStatusCode.BadRequest);

        public static readonly ErrorCodes ApiAccountNotFoundError = ErrorCodes.Create("api_account_not_found_error", HttpStatusCode.BadRequest);
    }
}
