using System;

namespace Reconciliation.Api.Models.Exception
{
    [Serializable]
    public class ApiException: System.Exception
    {
        public ApiException(ErrorCodes errorCodes, string message)
            : base(message)
        {
            ErrorCodes = errorCodes;
        }
        
        public ApiException(string service, ErrorCodes errorCodes, string message)
            : this(errorCodes, message)
        {
            Service = service;
        }
        
        public ErrorCodes ErrorCodes { get; }
        
        public string Service { get; }
    }
}
