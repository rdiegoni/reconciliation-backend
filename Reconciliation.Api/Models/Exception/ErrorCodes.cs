using System.Net;

namespace Reconciliation.Api.Models.Exception
{
    public class ErrorCodes
    {
        public static ErrorCodes Create(string errorCode, HttpStatusCode statusCode)
        {
            return new ErrorCodes
            {
                ErrorCode = errorCode,
                StatusCode = statusCode
            };
        }
        
        public string ErrorCode { get; internal set; }
        
        public HttpStatusCode StatusCode { get; internal set; }
    }
}
