using System.Collections.Generic;
using Newtonsoft.Json;

namespace Reconciliation.Api.Models.Exception
{
    public class FailureResponse
    {
        [JsonIgnore]
        public int StatusCode { get; set; }
        
        [JsonIgnore]
        public string Layer { get; set; }
        
        [JsonIgnore]
        public string Service { get; set; }
        
        [JsonIgnore]
        public string Action { get; set; }
        
        [JsonIgnore]
        public string ExternalErrorCode { get; set; }
        
        public string ErrorCode { get; set; }

        public string ErrorDescription { get; set; }
        
        [JsonIgnore]
        public string Request { get; set; }

        [JsonIgnore]
        public string Response { get; set; }
        
        public IDictionary<string, string[]> ErrorDetails { get; set; } 
        
        public IEnumerable<string> Actions { get; set; }
    }
}
