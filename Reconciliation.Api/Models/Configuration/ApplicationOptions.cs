namespace Reconciliation.Api.Models.Configuration
{
    public class ApplicationOptions
    {
        public string AccountPath { get; set; }

        public string TransactionPath { get; set; }
    }
}
