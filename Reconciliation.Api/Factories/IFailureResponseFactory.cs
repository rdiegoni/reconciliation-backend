using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Reconciliation.Api.Models.Exception;

namespace Reconciliation.Api.Factories
{
    public interface IFailureResponseFactory
    {
        FailureResponse CreateFailureResponse(ModelStateDictionary modelState);
        
        FailureResponse CreateFailureResponse(Exception exception);
    }
}
