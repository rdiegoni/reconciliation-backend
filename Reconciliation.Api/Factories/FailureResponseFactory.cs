using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Reconciliation.Api.Models.Exception;
using Errors = Reconciliation.Api.Models.Exception.Errors;

namespace Reconciliation.Api.Factories
{
    public class FailureResponseFactory: IFailureResponseFactory
    {
        public FailureResponse CreateFailureResponse(ModelStateDictionary modelState)
        {
            var errors = modelState
                .Where(e => e.Value.Errors.Count > 0)
                .ToDictionary(x => x.Key, x => x.Value.Errors.Select(error => error.ErrorMessage).ToArray());
            
            return new FailureResponse
            {
                StatusCode = (int)HttpStatusCode.BadRequest,
                ErrorCode = Errors.ApiParametersError.ErrorCode,
                ErrorDescription = "Invalid parameters",
                ErrorDetails = errors
            };
        }
        
        public FailureResponse CreateFailureResponse(Exception exception)
        {
            var currentException = exception is AggregateException aggregateException
                ? aggregateException.InnerException
                : exception;
            
            FailureResponse failureResponse = null;

            if (currentException is ApiException apiException)
            {
                failureResponse = CreateApiFailureResponse(apiException);
            } 

            if (failureResponse == null)
            {
                failureResponse = CreateUnHandleFailureResponse(currentException);
            }
            
            return failureResponse;
        }

        private static FailureResponse CreateApiFailureResponse(ApiException exception)
        {
            return new FailureResponse
            {
                Layer = "api",
                Service = exception.Service,
                StatusCode = (int)exception.ErrorCodes.StatusCode,
                ErrorCode = exception.ErrorCodes.ErrorCode,
                ErrorDescription = exception.Message ?? "An Api exception has been thrown.",
                ErrorDetails = GetErrorDetails(exception)
            };
        }

        private static FailureResponse CreateUnHandleFailureResponse(Exception exception)
        {
            return new FailureResponse
            {
                Layer = "api",
                StatusCode = (int) Errors.ApiInternalServerError.StatusCode,
                ErrorCode = Errors.ApiInternalServerError.ErrorCode,
                ErrorDescription = exception?.Message ?? "An unhandled exception has been thrown",
                ErrorDetails = GetErrorDetails(exception)
            };
        }
        
        private static Dictionary<string, string[]> GetErrorDetails(Exception exception)
        {
            var message = exception?.InnerException?.Message ?? exception?.Message;
            if (string.IsNullOrEmpty(message))
            {
                return null;
            }

            return new Dictionary<string, string[]>
            {
                { "message", new[] { message } }
            };
        }
    }
}
