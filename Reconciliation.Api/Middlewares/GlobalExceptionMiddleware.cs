using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Reconciliation.Api.Factories;

namespace Reconciliation.Api.Middlewares
{
    public class GlobalExceptionMiddleware
    {
        private readonly RequestDelegate _next;  
        
        public GlobalExceptionMiddleware(RequestDelegate next) {  
            _next = next;  
        }  
        
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static async Task HandleExceptionAsync(
            HttpContext context, 
            Exception ex)
        {
            var failureResponseFactory = context.RequestServices.GetService<IFailureResponseFactory>();
                
            var failureResponse = failureResponseFactory.CreateFailureResponse(ex);
            
            context.Items.Add("ErrorCode", failureResponse.ErrorCode);
            context.Items.Add("ErrorDescription", failureResponse.ErrorDescription);
            context.Items.Add("ErrorDetails", JsonConvert.SerializeObject(failureResponse.ErrorDetails));

            context.Response.Headers.Add("Access-Control-Allow-Origin", context.Request.Headers["Origin"]);
            context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");

            var serializedResponse = JsonConvert.SerializeObject(failureResponse);
            context.Response.StatusCode = failureResponse.StatusCode;
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(serializedResponse);
        }
    }
}
