using Microsoft.AspNetCore.Builder;

namespace Reconciliation.Api.Middlewares
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<GlobalExceptionMiddleware>();
        }
    }
}
