using System.Collections.Generic;
using System.Threading.Tasks;
using AccountDomain = Reconciliation.Domain.Account.Account;
using TransactionDomain = Reconciliation.Domain.Account.Entities.Transaction;

namespace Reconciliation.Api.Services.Account
{
    public interface IAccountService
    {
        Task Load();
        Task<IList<AccountDomain>> GetAccounts();
        Task<AccountDomain> GetAccount(string accountId);
        Task<IList<TransactionDomain>> GetTransactions(string accountId);
    }
}
