using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reconciliation.Api.Models.Exception;
using Reconciliation.Domain;
using Reconciliation.Infrastructure.Transaction;
using AccountDomain = Reconciliation.Domain.Account.Account;
using TransactionDomain = Reconciliation.Domain.Account.Entities.Transaction;

namespace Reconciliation.Api.Services.Account
{
    public class AccountService: IAccountService
    {
        private readonly IRepository<AccountDomain> _accountRepository;
        private readonly Infrastructure.Account.IAccountService _accountServiceApi;
        private readonly ITransactionService _transactionServiceApi;

        public AccountService(
            Infrastructure.Account.IAccountService accountService,
            ITransactionService transactionService,
            IRepository<AccountDomain> accountRepository)
        {
            _accountServiceApi = accountService;
            _transactionServiceApi = transactionService;
            _accountRepository = accountRepository;
        }

        public async Task Load()
        {
            var accounts = await _accountServiceApi.GetAccounts();
            var transactions = await _transactionServiceApi.GetTransactions();

            foreach (var account in accounts)
            {
                var accountTransactions = transactions
                    .Where(x => x.AccountId == account.Id)
                    .ToList();
                account.AddTransactions(accountTransactions);
            }

            if (accounts.Any())
            {
                await _accountRepository.Add(accounts);
            }
        }
        
        public async Task<IList<AccountDomain>> GetAccounts()
        {
            var accounts = await _accountRepository.GetEntities();
            return accounts;
        }
        
        public async Task<AccountDomain> GetAccount(string accountId)
        {
            var accounts = await _accountRepository.GetEntities();
            
            var account = accounts.FirstOrDefault(account => account.Id == accountId);
            if (account == null)
            {
                throw new ApiException(Errors.ApiAccountNotFoundError, $"Account {accountId} not found");
            }

            return account;
        }
        
        public async Task<IList<TransactionDomain>> GetTransactions(string accountId)
        {
            var accounts = await _accountRepository.GetEntities();
            
            var account = accounts.FirstOrDefault(account => account.Id == accountId);
            if (account == null)
            {
                throw new ApiException(Errors.ApiAccountNotFoundError, $"Account {accountId} not found");
            }
            
            return account.Transactions.ToList();
        }
    }
}
