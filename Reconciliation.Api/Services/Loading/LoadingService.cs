using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Reconciliation.Api.Services.Account;

namespace Reconciliation.Api.Services.Loading
{
    public class StartServices: IHostedService
    {
        private readonly IServiceProvider _serviceProvider;

        public StartServices(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();
            var accountService = scope.ServiceProvider.GetService<IAccountService>();

            try
            {
                await accountService.Load();
            }
            catch (Exception ex)
            {
                await Console.Error.WriteLineAsync("Something went wrong when hosted service was starting: " + ex.Message);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
