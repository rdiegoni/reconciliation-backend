using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reconciliation.Api.Converters;
using Reconciliation.Api.Models.Entity;
using Reconciliation.Api.Services.Account;

namespace Reconciliation.Api.Controllers
{
    [ApiController]
    public class AccountController
    {
        private readonly IAccountService _accountService;
        
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        [HttpGet]
        [Route("/accounts")]
        public async Task<IEnumerable<Account>> GetAccounts()
        {
            var accounts = await _accountService.GetAccounts();
            return accounts.Select(AccountConverter.Convert).ToList();
        }
        
        [Route("/accounts/{accountId}")]
        [HttpGet]
        public async Task<Account> GetAccount(string accountId)
        {
            var account = await _accountService.GetAccount(accountId);
            return AccountConverter.Convert(account);
        }
        
        [Route("/accounts/{accountId}/transactions")]
        [HttpGet]
        public async Task<IList<Transaction>> GetTransactions(string accountId)
        {
            var transactions = await _accountService.GetTransactions(accountId);
            return transactions.Select(TransactionConverter.Convert).ToList();
        }
    }
}
