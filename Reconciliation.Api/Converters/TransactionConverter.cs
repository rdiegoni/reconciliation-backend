using System;
using Reconciliation.Domain.Account.Entities;
using Transaction = Reconciliation.Api.Models.Entity.Transaction;
using TransactionDomain = Reconciliation.Domain.Account.Entities.Transaction;

namespace Reconciliation.Api.Converters
{
    public static class TransactionConverter
    {
        public static Transaction Convert(TransactionDomain transaction)
        {
            return new Transaction
            {
                Id = transaction.Id,
                AccountId = transaction.AccountId,
                Currency = transaction.Currency,
                Amount = transaction.Amount,
                Type = Enum.GetName(typeof(TransactionType), transaction.Type),
                Date = transaction.Date,
            };
        }
    }
}
