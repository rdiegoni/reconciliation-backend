using System.Linq;
using Reconciliation.Api.Models.Entity;
using AccountDomain = Reconciliation.Domain.Account.Account;

namespace Reconciliation.Api.Converters
{
    public static class AccountConverter
    {
        public static Account Convert(AccountDomain account)
        {
            var balances = account.Balances
                .Select(x => new Balance { Amount = x.Amount, Currency = x.Currency })
                .ToList();
            return new Account { Id = account.Id, Name = account.Name, Balances = balances };
        }
    }
}
