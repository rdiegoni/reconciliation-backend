using System.Collections.Generic;
using System.Linq;
using Reconciliation.Domain.Account.Entities;

namespace Reconciliation.Domain.Account
{
    public class Account
    {
        public Account(string id, string name)
        {
            Id = id;
            Name = name;
        }
        
        public string Id { get; }

        public string Name { get; }

        public IList<Transaction> Transactions { get; private set; } = new List<Transaction>();

        public void AddTransactions(IEnumerable<Transaction> transactions)
        {
            Transactions = new List<Transaction>(transactions);
        }

        public IList<Balance> Balances
        {
            get
            {
                return Transactions
                    .GroupBy(x => x.Currency)
                    .Select(x => CreateBalance(x.Key, x))
                    .ToList();
            }
        }

        private static Balance CreateBalance(string currency, IEnumerable<Transaction> transactions)
        {
            var amount = transactions.Sum(y => y.Type != TransactionType.Settled ? y.Amount * -1 : y.Amount);
            return new Balance(currency, amount);
        }
    }
}
