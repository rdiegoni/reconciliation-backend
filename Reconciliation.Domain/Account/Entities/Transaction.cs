using System;

namespace Reconciliation.Domain.Account.Entities
{
    public class Transaction
    {
        public Transaction(string id, string accountId, decimal amount, string currency, string type, DateTimeOffset date)
        {
            Id = id;
            AccountId = accountId;
            Amount = amount;
            Currency = currency;
            Type = Enum.Parse<TransactionType>(type);
            Date = date;
        }
        
        public string Id { get; }

        public string AccountId { get; }
        
        public decimal Amount { get; }
        
        public string Currency { get; }
        
        public TransactionType Type { get; }
        
        public DateTimeOffset Date { get; }
    }
}
