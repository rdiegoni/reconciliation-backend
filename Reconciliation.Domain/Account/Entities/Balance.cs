namespace Reconciliation.Domain.Account.Entities
{
    public class Balance
    {
        public Balance(string currency, decimal amount)
        {
            Amount = amount;
            Currency = currency;
        }
        
        public decimal Amount { get; }

        public string Currency { get; }
    }
}
