using System.ComponentModel;

namespace Reconciliation.Domain.Account.Entities
{
    public enum TransactionType
    {
        None,
        [Description("Chargeback")]
        Chargeback,
        [Description("Refunded")]
        Refunded,
        [Description("Settled")]
        Settled,
    }
}
