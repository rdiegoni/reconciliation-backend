using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reconciliation.Domain
{
    public interface IRepository<TEntity>
    {
        Task<IList<TEntity>> GetEntities();
        
        Task Add(IEnumerable<TEntity> entities);
    }
}
