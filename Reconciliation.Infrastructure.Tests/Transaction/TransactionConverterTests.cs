using AutoFixture;
using AutoFixture.AutoMoq;
using Reconciliation.Domain.Account.Entities;
using Reconciliation.Infrastructure.Transaction;
using Xunit;

namespace Reconciliation.Infrastructure.Tests.Transaction
{
    public class TransactionConverterTests
    {
        private readonly IFixture _fixture;

        public TransactionConverterTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
        }
        
        [Fact]
        public void Convert_WhenInvoked_ShouldReturnTheConvertedList()
        {
            // Given
            var dto = _fixture
                .Build<Infrastructure.Transaction.Transaction>()
                .With(x => x.Type, "Settled")
                .Create();
            
            // When
            var result = TransactionConverter.Convert(dto);

            // Then
            Assert.Equal(dto.Id, result.Id);
            Assert.Equal(dto.AccountId, result.AccountId);
            Assert.Equal(dto.Currency, result.Currency);
            Assert.Equal(dto.Amount, result.Amount);
            Assert.Equal(dto.Datetime, result.Date);
            Assert.Equal(TransactionType.Settled, result.Type);
        }
    }
}
