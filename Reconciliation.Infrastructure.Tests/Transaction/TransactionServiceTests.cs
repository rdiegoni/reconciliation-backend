using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Reconciliation.Infrastructure.Providers;
using Reconciliation.Infrastructure.Tests.Asserts;
using Reconciliation.Infrastructure.Transaction;
using Xunit;

namespace Reconciliation.Infrastructure.Tests.Transaction
{
    public class TransactionServiceTests
    {
        private readonly IFixture _fixture;

        public TransactionServiceTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
        }
        
        [Fact]
        public async Task GetEntities_WhenEntitiesDoNotExist_ShouldReturnAnEmptyList()
        {
            // Given
            var entities = _fixture.CreateMany<Infrastructure.Transaction.Transaction>(0);
            
            var providerMock = _fixture.Freeze<Mock<IProvider<Infrastructure.Transaction.Transaction>>>();
            providerMock.Setup(x => x.GetEntities()).Returns(Task.FromResult(entities));
            
            var subject = _fixture.Create<TransactionService>();
            
            // When
            var result = await subject.GetTransactions();
            
            // Then
            Assert.Empty(result);
        }
        
        [Fact]
        public async Task GetEntities_WhenEntitiesDoExist_ShouldReturnThem()
        {
            // Given
            var entities = _fixture.Build<Infrastructure.Transaction.Transaction>()
                .With(x => x.Type, "Settled")
                .CreateMany();
            
            var providerMock = _fixture.Freeze<Mock<IProvider<Infrastructure.Transaction.Transaction>>>();
            providerMock.Setup(x => x.GetEntities()).Returns(Task.FromResult(entities));
            
            var subject = _fixture.Create<TransactionService>();
            
            // When
            var result = await subject.GetTransactions();
            
            // Then
            TransactionAssert.Assert(entities, result);
        }
    }
}
