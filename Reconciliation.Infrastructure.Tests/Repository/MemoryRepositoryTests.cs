using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Reconciliation.Infrastructure.Repositories;
using Reconciliation.Infrastructure.Tests.Asserts;
using Xunit;

namespace Reconciliation.Infrastructure.Tests.Repository
{
    public class MemoryRepositoryTests
    {
        private readonly IFixture _fixture;

        public MemoryRepositoryTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
        }

        [Fact]
        public async Task GetEntities_WhenEntitiesDoNotExist_ShouldReturnAnEmptyList()
        {
            // Given    
            var keyCache = _fixture.Create<string>();

            var options = Options.Create(new MemoryDistributedCacheOptions());
            var memoryCache = new MemoryDistributedCache(options);

            var subject = new MemoryRepository<Domain.Account.Account>(memoryCache, keyCache);

            // When
            var result = await subject.GetEntities();

            // Then
            Assert.Empty(result);
        }
        
        [Fact]
        public async Task GetEntities_WhenEntitiesDoExist_ShouldReturnThem()
        {
            // Given    
            var keyCache = _fixture.Create<string>();

            var options = Options.Create(new MemoryDistributedCacheOptions());
            var memoryCache = new MemoryDistributedCache(options);
            
            var entities = _fixture.CreateMany<Domain.Account.Account>();
            await memoryCache.SetStringAsync(keyCache, JsonConvert.SerializeObject(entities));

            var subject = new MemoryRepository<Domain.Account.Account>(memoryCache, keyCache);

            // When
            var result = await subject.GetEntities();

            // Then
            AccountAssert.Assert(entities, result);
        }
        
        [Fact]
        public async Task Add_WhenEntitiesDoExistForThatKey_ShouldStoreThem()
        {
            // Given    
            var keyCache = _fixture.Create<string>();

            var options = Options.Create(new MemoryDistributedCacheOptions());
            var memoryCache = new MemoryDistributedCache(options);

            var entities = _fixture.CreateMany<Domain.Account.Account>();

            var subject = new MemoryRepository<Domain.Account.Account>(memoryCache, keyCache);

            // When
            await subject.Add(entities);
            
            // Then
            var value = await memoryCache.GetStringAsync(keyCache);
            var result = JsonConvert.DeserializeObject<IEnumerable<Domain.Account.Account>>(value);
            
            AccountAssert.Assert(entities, result);
        }

        [Fact]
        public async Task Add_WhenEntitiesDoExistForThatKey_ShouldOverrideThem()
        {
            // Given    
            var keyCache = _fixture.Create<string>();

            var options = Options.Create(new MemoryDistributedCacheOptions());
            var memoryCache = new MemoryDistributedCache(options);

            var initialEntities = _fixture.CreateMany<Domain.Account.Account>();
            var initialEntitiesString = JsonConvert.SerializeObject(initialEntities);
            await memoryCache.SetStringAsync(keyCache, initialEntitiesString);

            var entities = _fixture.CreateMany<Domain.Account.Account>();
            var subject = new MemoryRepository<Domain.Account.Account>(memoryCache, keyCache);

            // When
            await subject.Add(entities);
            
            // Then
            var entitiesString = await memoryCache.GetStringAsync(keyCache);
            var result = JsonConvert.DeserializeObject<IEnumerable<Domain.Account.Account>>(entitiesString);
            
            Assert.NotEqual(initialEntitiesString, entitiesString);
            AccountAssert.Assert(entities, result);
        }
    }
}
