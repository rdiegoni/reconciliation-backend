using System.Collections.Generic;
using System.Linq;

namespace Reconciliation.Infrastructure.Tests.Asserts
{
    public static class AccountAssert
    {
        public static void Assert(Infrastructure.Account.Account expected, Domain.Account.Account result)
        {
            Xunit.Assert.Equal(expected.Id, result.Id);
            Xunit.Assert.Equal(expected.Name, result.Name);
            Xunit.Assert.Empty(result.Balances);
            Xunit.Assert.Empty(result.Transactions);
        }
     
        public static void Assert(IEnumerable<Infrastructure.Account.Account> expected, IEnumerable<Domain.Account.Account> result)
        {
            var accountsExpected = expected.ToList();
            var accountsResult = result.ToList();
            
            for (var i = 0; i < accountsExpected.Count(); i++)
            {
                var accountExpected = accountsExpected.ElementAt(i);
                var accountResult = accountsResult.ElementAt(i);
                Assert(accountExpected, accountResult);
            }
        }
        
        public static void Assert(Domain.Account.Account expected, Domain.Account.Account result)
        {
            Xunit.Assert.Equal(expected.Id, result.Id);
            Xunit.Assert.Equal(expected.Name, result.Name);
            Xunit.Assert.Equal(expected.Balances, result.Balances);
            Xunit.Assert.Equal(expected.Transactions, result.Transactions);
        }
        
        public static void Assert(IEnumerable<Domain.Account.Account> expected, IEnumerable<Domain.Account.Account> result)
        {
            var accountsExpected = expected.ToList();
            var accountsResult = result.ToList();
            
            for (var i = 0; i < accountsExpected.Count(); i++)
            {
                var accountExpected = accountsExpected.ElementAt(i);
                var accountResult = accountsResult.ElementAt(i);
                Assert(accountExpected, accountResult);
            }
        }
    }
}
