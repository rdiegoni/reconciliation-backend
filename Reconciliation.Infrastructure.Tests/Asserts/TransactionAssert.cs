using System.Collections.Generic;
using System.Linq;

namespace Reconciliation.Infrastructure.Tests.Asserts
{
    public static class TransactionAssert
    {
        public static void Assert(Infrastructure.Transaction.Transaction expected, Domain.Account.Entities.Transaction result)
        {
            Xunit.Assert.Equal(expected.Id, result.Id);
            Xunit.Assert.Equal(expected.AccountId, result.AccountId);
            Xunit.Assert.Equal(expected.Amount, result.Amount);
            Xunit.Assert.Equal(expected.Currency, result.Currency);
            Xunit.Assert.Equal(expected.Datetime, result.Date);
            Xunit.Assert.Equal(expected.Type, result.Type.ToString());
        }
     
        public static void Assert(
            IEnumerable<Infrastructure.Transaction.Transaction> expected, 
            IEnumerable<Domain.Account.Entities.Transaction> result)
        {
            var accountsExpected = expected.ToList();
            var accountsResult = result.ToList();
            
            for (var i = 0; i < accountsExpected.Count(); i++)
            {
                var accountExpected = accountsExpected.ElementAt(i);
                var accountResult = accountsResult.ElementAt(i);
                Assert(accountExpected, accountResult);
            }
        }
    }
}
