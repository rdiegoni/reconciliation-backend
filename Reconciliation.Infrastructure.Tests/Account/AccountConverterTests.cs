using AutoFixture;
using AutoFixture.AutoMoq;
using Reconciliation.Infrastructure.Account;
using Xunit;

namespace Reconciliation.Infrastructure.Tests.Account
{
    public class AccountConverterTests
    {
        private readonly IFixture _fixture;

        public AccountConverterTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
        }
        
        [Fact]
        public void Convert_WhenInvoked_ShouldReturnTheConvertedList()
        {
            // Given
            var dto = _fixture.Create<Infrastructure.Account.Account>();
            
            // When
            var result = AccountConverter.Convert(dto);

            // Then
            Assert.Equal(dto.Id, result.Id);
            Assert.Equal(dto.Name, result.Name);
        }
    }
}
