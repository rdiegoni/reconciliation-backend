using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Reconciliation.Infrastructure.Account;
using Reconciliation.Infrastructure.Providers;
using Reconciliation.Infrastructure.Tests.Asserts;
using Xunit;

namespace Reconciliation.Infrastructure.Tests.Account
{
    public class AccountServiceTests
    {
        private readonly IFixture _fixture;

        public AccountServiceTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
        }
        
        [Fact]
        public async Task GetEntities_WhenEntitiesDoNotExist_ShouldReturnAnEmptyList()
        {
            // Given
            var entities = _fixture.CreateMany<Infrastructure.Account.Account>(0);
            
            var providerMock = _fixture.Freeze<Mock<IProvider<Infrastructure.Account.Account>>>();
            providerMock.Setup(x => x.GetEntities()).Returns(Task.FromResult(entities));
            
            var subject = _fixture.Create<AccountService>();
            
            // When
            var result = await subject.GetAccounts();
            
            // Then
            Assert.Empty(result);
        }
        
        [Fact]
        public async Task GetEntities_WhenEntitiesDoExist_ShouldReturnThem()
        {
            // Given
            var entities = _fixture.CreateMany<Infrastructure.Account.Account>();
            
            var providerMock = _fixture.Freeze<Mock<IProvider<Infrastructure.Account.Account>>>();
            providerMock.Setup(x => x.GetEntities()).Returns(Task.FromResult(entities));
            
            var subject = _fixture.Create<AccountService>();
            
            // When
            var result = await subject.GetAccounts();
            
            // Then
            AccountAssert.Assert(entities, result);
        }
    }
}
