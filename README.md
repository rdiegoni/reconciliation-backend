# Reconciliation Backend

The reconciliation backend is a simple RESTFUL Api proposing to the user few actions against some accounts. The use can

- List of the accounts
- Get an account
- Get the list of the transactions for an account

## Prerequisite

[.Net Core v3.1](https://dotnet.microsoft.com/en-us/download/dotnet/3.1)

## How to run it

### Locally

To run the spa locally, you have to execute the following commands

- `dotnet build`
- `dotnet run --project Reconciliation.Api/Reconciliation.Api.csproj`

Open a browser with the url `http://localhost:5000/accounts` and check you get a list of accounts.

### Docker

To run the spa locally via docker, you have to execute the following commands

- `docker build -t reconciliation-api:1.0.0 -f Dockerfile .`
- `docker run -ti --rm --name reconciliation-api --publish 5000:5000 --detach reconciliation-api:1.0.0`

## How to run test

To run the test, execute the command
* `dotnet test`

## Architecture choices

### External libraries

For Json manipulation, I have decided to use [Newtonsoft](https://www.newtonsoft.com/json) which is a good one for that test. When performance is key, some equivalent such as System.Text.Json maybe better.

For unit testing, I have decided to use [XUnit](https://xunit.net/)

For better TDD experience, I have decided to use [AutoFixture](https://github.com/AutoFixture/AutoFixture)

### Project structure

The project has been designed to make the maintainability easy. I want from a user request to understand right away

- what is the problem
- where to go
- how to fix it

I decided to create 3 projects to separate the layer of my project

- `Reconciliation.Api` - Api layer - it contains all the logic the user of our service will expect in terms of features
- `Reconciliation.Domain` - Domain layer - it contains all the domain logic of the application. It follows the same principles used in the DDD approach
- `Reconciliation.Infrastructure` - Infrastructure layer - it contains all the logic to get the data needed

Each project can now manage everything in an isolated way like error code, monitoring, tracking, etc. For example, is a user get an error code `infrastructure_account_file_not_found`, we will know right away we have an issue in the service `AccountService` in the directory `Reconciliation.Infrastructure > Account`

Of course, you need to know the role of every patterns you decided to use. Happy to talk in real as it will be too long to explain everything here.

### Language

To develop the backend, I decided to use

- `.Net Core/C#` - it is my main backend language and working in investment banking I had the proof we can have performant complex system that can answer any requests within 200ms

### Choices

### Patterns

In order to build a clean code architecture, I have used multiple patterns

- `Factory method` - use to create the right response based on the type of exception (i.e api, domain or infrastructure exception)
- `Repository` - use to store, in memory, the data loaded from the system files
- `Service` - use to manage some logic the domain is not responsible for

### Exception

I have always been convinced that managing exception in the business logic bring more than noise and can be extracted somewhere else. In that project, I have created a middleware that catch all the exception triggered in the code and by analysing the exception details you can return the right answer (code, message, etc). Of course this approach is still in experimentation on my side but I'm working on it and I'll be happy to share it in an open source way. The benefit of that approach is your code will not contains any try/catch anymore and so focus only on the business logic.

### Data

It is definitely bad practice to load the data on every request. So, for that project, I decided to use a background task that will load, in memory, the different data when the service start. Then, all requests will get the right answer based on these data in memory.

I choose that solution because the size of the data was not huge but multiple improvements can be brought

- reload the data every x seconds/minutes
- reload the data on demand (i.e via webhook, lambda, etc)
- store the data in the cloud, database, other
- etc

#### Dto

In order to minimize the impacts of things your spa do not own, I try as much as possible to isolate these parts. For example, in that project I needed to communicate with the IO stream in order to read the data from some system file. Instead of creating a model that match the file structure and use it directly, I have followed the process

- Create a DTO that has the same signature as the file
- Create a model that contains a signature matching my business expectation
- Create a converter that will convert the DTO to my model

By respecting that process, you have isolated what you do not own so any changes on the backend will be done in few seconds on the spa side.

#### Other

I'm happy to talk about the solution in real as they are a lot of to say and I may have forgotten to explain few of them

## Improvements

Multiple improvements could have been done on that project

- for loading the data, go to the `Data` section above
- add logging to be able to investigate any issues
- improve error management with more granular error codes/messages
- add some performance KPI to keep an eye on the response time based on concurrent scenario (e.g 1, 50, 100 users) - in a more general way implement some observability KPI
- Add monitoring by using Prometheus for example in order to have an eyes one some KPI and be alerted in case of issues
- Add incident management by using PagerDuty in order to fix issue in a more agile way
- Implement a disaster crash recovery strategy (e.g retry to load data if failed the current attempt, etc)
- Deploy the solution in the cloud such as Heroku, AWS, etc
