using AutoFixture;
using AutoFixture.AutoMoq;
using Reconciliation.Api.Converters;
using Reconciliation.Api.Tests.Asserts;
using Reconciliation.Domain.Account;
using Xunit;

namespace Reconciliation.Api.Tests.Converters
{
    public class AccountConverterTests
    {
        private readonly IFixture _fixture;

        public AccountConverterTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
        }

        [Fact]
        public void Convert_WhenInvoked_ShouldReturnAnAccountDto()
        {
            // Given
            var account = _fixture.Create<Account>();
            
            // When
            var result = AccountConverter.Convert(account);

            // Then
            AccountAssert.Assert(account, result);
        }
    }
}
