using AutoFixture;
using AutoFixture.AutoMoq;
using Reconciliation.Api.Converters;
using Reconciliation.Api.Tests.Asserts;
using Reconciliation.Api.Tests.AutoFixture;
using Reconciliation.Domain.Account.Entities;
using Xunit;

namespace Reconciliation.Api.Tests.Converters
{
    public class TransactionConverterTests
    {
        private readonly IFixture _fixture;

        public TransactionConverterTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            _fixture.Customizations.Add(new TransactionTypeBuilder());
        }

        [Fact]
        public void Convert_WhenInvoked_ShouldReturnATransactionDto()
        {
            // Given
            var transaction = _fixture.Create<Transaction>();
            
            // When
            var result = TransactionConverter.Convert(transaction);

            // Then
            TransactionAssert.Assert(transaction, result);
        }
    }
}
