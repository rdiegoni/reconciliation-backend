using System;
using System.Linq;
using System.Reflection;
using AutoFixture.Kernel;
using Reconciliation.Domain.Account.Entities;

namespace Reconciliation.Api.Tests.AutoFixture
{
    internal class TransactionTypeBuilder : ISpecimenBuilder
    {
        private readonly TransactionType[] _values;
        private int _i;

        internal TransactionTypeBuilder()
        {
            _values =
                Enum.GetValues(typeof(TransactionType))
                    .Cast<TransactionType>()
                    .Where(x => x != TransactionType.None)
                    .ToArray();
        }

        public object Create(object request, ISpecimenContext context)
        {
            if (request is ParameterInfo { Name: "type" })
            {
                return _values[_i == _values.Length - 1 ? _i = 0 : ++_i].ToString();
            }

            return new NoSpecimen();
        }
    }
}
