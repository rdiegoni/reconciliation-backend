using System.Collections.Generic;
using System.Linq;
using Reconciliation.Api.Models.Entity;

namespace Reconciliation.Api.Tests.Asserts
{
    public static class TransactionAssert
    {
        public static void Assert(Domain.Account.Entities.Transaction expected, Transaction result)
        {
            Xunit.Assert.Equal(expected.Id, result.Id);
            Xunit.Assert.Equal(expected.AccountId, result.AccountId);
            Xunit.Assert.Equal(expected.Amount, result.Amount);
            Xunit.Assert.Equal(expected.Currency, result.Currency);
            Xunit.Assert.Equal(expected.Date, result.Date);
            Xunit.Assert.Equal(expected.Type.ToString(), result.Type);
        }

        public static void Assert(IEnumerable<Domain.Account.Entities.Transaction> expected, IEnumerable<Transaction> result)
        {
            var transactionsExpected = expected.ToList();
            var transactionsResult = result.ToList();
            
            for (var i = 0; i < transactionsExpected.Count(); i++)
            {
                var transactionExpected = transactionsExpected.ElementAt(i);
                var transactionResult = transactionsResult.ElementAt(i);
                Assert(transactionExpected, transactionResult);
            }
        }
    }
}
