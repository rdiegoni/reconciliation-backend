using System.Collections.Generic;
using System.Linq;
using Reconciliation.Api.Models.Entity;

namespace Reconciliation.Api.Tests.Asserts
{
    public static class AccountAssert
    {
        public static void Assert(Domain.Account.Account expected, Account result)
        {
            Xunit.Assert.Equal(expected.Id, result.Id);
            Xunit.Assert.Equal(expected.Name, result.Name);
            Xunit.Assert.Equal(expected.Balances.Count, result.Balances.Count());
            
            for (var i = 0; i < expected.Balances.Count; i++)
            {
                var expectedBalance = expected.Balances.ElementAt(i);
                var resultBalance = result.Balances.ElementAt(i);
                Xunit.Assert.Equal(expectedBalance.Amount, resultBalance.Amount);
                Xunit.Assert.Equal(expectedBalance.Currency, resultBalance.Currency);
            }
        }
        
        public static void Assert(IEnumerable<Domain.Account.Account> expected, IEnumerable<Account> result)
        {
            var accountsExpected = expected.ToList();
            var accountsResult = result.ToList();
            
            for (var i = 0; i < accountsExpected.Count(); i++)
            {
                var accountExpected = accountsExpected.ElementAt(i);
                var accountResult = accountsResult.ElementAt(i);
                Assert(accountExpected, accountResult);
            }
        }
    }
}
