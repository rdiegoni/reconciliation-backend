using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Reconciliation.Api.Models.Exception;
using Reconciliation.Api.Services.Account;
using Reconciliation.Api.Tests.AutoFixture;
using Reconciliation.Domain;
using Reconciliation.Domain.Account.Entities;
using Xunit;
using IAccountServiceApi = Reconciliation.Infrastructure.Account.IAccountService;

namespace Reconciliation.Api.Tests.Services.Account
{
    public class AccountServiceTests
    {
        private readonly IFixture _fixture;

        public AccountServiceTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            _fixture.Customizations.Add(new TransactionTypeBuilder());
        }
        
        [Fact]
        public async Task Load_WhenAccountsDoNotExist_ShouldDoNothing()
        {
            // Given
            IList<Domain.Account.Account> accounts = Array.Empty<Domain.Account.Account>();
            var accountServiceApiMock = _fixture.Freeze<Mock<IAccountServiceApi>>();
            accountServiceApiMock.Setup(x => x.GetAccounts()).Returns(Task.FromResult(accounts));
            
            var repositoryMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            
            var subject = _fixture.Create<AccountService>();

            // When
            await subject.Load();

            // Then
            repositoryMock.Verify(x => x.Add(It.IsAny<IEnumerable<Domain.Account.Account>>()), Times.Never);
        }
        
        [Fact]
        public async Task Load_WhenAccountsWithoutTransactionsDoExist_ShouldAddThem()
        {
            // Given
            IList<Domain.Account.Account> accounts = _fixture.CreateMany<Domain.Account.Account>().ToList();
            var accountServiceApiMock = _fixture.Freeze<Mock<IAccountServiceApi>>();
            accountServiceApiMock.Setup(x => x.GetAccounts()).Returns(Task.FromResult(accounts));

            var repositoryMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            
            var subject = _fixture.Create<AccountService>();

            // When
            await subject.Load();

            // Then
            repositoryMock.Verify(x => x.Add(accounts), Times.Once);
        }

        [Fact]
        public async Task Load_WhenAccountsWithTransactionsDoExist_ShouldAddThem()
        {
            // Given
            IList<Domain.Account.Account> accounts = _fixture.CreateMany<Domain.Account.Account>().ToList();
            foreach (var account in accounts)
            {
                account.AddTransactions(_fixture.CreateMany<Transaction>());
            }

            var accountServiceApiMock = _fixture.Freeze<Mock<IAccountServiceApi>>();
            accountServiceApiMock.Setup(x => x.GetAccounts()).Returns(Task.FromResult(accounts));

            var repositoryMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            
            var subject = _fixture.Create<AccountService>();

            // When
            await subject.Load();

            // Then
            repositoryMock.Verify(x => x.Add(accounts), Times.Once);
        }
        
        [Fact]
        public async Task GetAccounts_WhenAccountsDoNotExist_ShouldReturnAnEmptyList()
        {
            // Given

            var subject = _fixture.Create<AccountService>();

            // When
            var result = await subject.GetAccounts();

            // Then
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetAccounts_WhenAccountsDoExist_ShouldReturnThem()
        {
            // Given
            IList<Domain.Account.Account> accounts = _fixture.CreateMany<Domain.Account.Account>().ToList();
            
            var accountServiceApiMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            accountServiceApiMock.Setup(x => x.GetEntities())
                .Returns(Task.FromResult(accounts));

            var subject = _fixture.Create<AccountService>();

            // When
            var result = await subject.GetAccounts();

            // Then
            Assert.True(accounts.SequenceEqual(result));
        }
        
        [Fact]
        public async Task GetAccount_WhenAccountDoesNotExist_ShouldThrowException()
        {
            // Given
            var fakeAccountId = _fixture.Create<string>();
            var account = _fixture.Create<Domain.Account.Account>();
            
            var accountServiceApiMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            accountServiceApiMock.Setup(x => x.GetEntities())
                .Returns(Task.FromResult(new[] { account } as IList<Domain.Account.Account>));

            var subject = _fixture.Create<AccountService>();

            // When
            var exception = await Assert.ThrowsAsync<ApiException>(() => subject.GetAccount(fakeAccountId));

            // Then
            Assert.Equal(Errors.ApiAccountNotFoundError, exception.ErrorCodes);
            Assert.Equal($"Account {fakeAccountId} not found", exception.Message);
        }
        
        [Fact]
        public async Task GetAccount5_WhenAccountDoesExist_ShouldReturnIt()
        {
            // Given
            var account = _fixture.Create<Domain.Account.Account>();
            
            var accountServiceApiMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            accountServiceApiMock.Setup(x => x.GetEntities())
                .Returns(Task.FromResult(new[] { account } as IList<Domain.Account.Account>));

            var subject = _fixture.Create<AccountService>();

            // When
            var result = await subject.GetAccount(account.Id);

            // Then
            Assert.Equal(account, result);
        }
        
        [Fact]
        public async Task GetTransactions_WhenAccountDoesNotExist_ShouldThrowException()
        {
            // Given
            var fakeAccountId = _fixture.Create<string>();
            var account = _fixture.Create<Domain.Account.Account>();
            
            var accountServiceApiMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            accountServiceApiMock.Setup(x => x.GetEntities())
                .Returns(Task.FromResult(new[] { account } as IList<Domain.Account.Account>));

            var subject = _fixture.Create<AccountService>();

            // When
            var exception = await Assert.ThrowsAsync<ApiException>(() => subject.GetTransactions(fakeAccountId));

            // Then
            Assert.Equal(Errors.ApiAccountNotFoundError, exception.ErrorCodes);
            Assert.Equal($"Account {fakeAccountId} not found", exception.Message);
        }
        
        [Fact]
        public async Task GetTransactions_WhenAccountDoesExistAndHasNoTransactions_ShouldReturnAnEmptyList()
        {
            // Given
            var account = _fixture.Create<Domain.Account.Account>();
            
            var accountServiceApiMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            accountServiceApiMock.Setup(x => x.GetEntities())
                .Returns(Task.FromResult(new[] { account } as IList<Domain.Account.Account>));

            var subject = _fixture.Create<AccountService>();

            // When
            var result = await subject.GetTransactions(account.Id);

            // Then
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetTransactions_WhenAccountDoesExistAndHasTransactions_ShouldReturnThem()
        {
            // Given
            var transactions = _fixture.CreateMany<Transaction>();
            var account = _fixture.Create<Domain.Account.Account>();
            account.AddTransactions(transactions);
            
            var accountServiceApiMock = _fixture.Freeze<Mock<IRepository<Domain.Account.Account>>>();
            accountServiceApiMock.Setup(x => x.GetEntities())
                .Returns(Task.FromResult(new[] { account } as IList<Domain.Account.Account>));

            var subject = _fixture.Create<AccountService>();

            // When
            var result = await subject.GetTransactions(account.Id);

            // Then
            Assert.True(account.Transactions.SequenceEqual(result));
        }
    }
}
