using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using Reconciliation.Api.Controllers;
using Reconciliation.Api.Services.Account;
using Reconciliation.Api.Tests.Asserts;
using Reconciliation.Api.Tests.AutoFixture;
using Reconciliation.Domain.Account;
using Reconciliation.Domain.Account.Entities;
using Xunit;

namespace Reconciliation.Api.Tests.Controllers
{
    public class AccountControllerTests
    {
        private readonly IFixture _fixture;

        public AccountControllerTests()
        {
            _fixture = new Fixture()
                .Customize(new AutoMoqCustomization());
            _fixture.Customizations.Add(new TransactionTypeBuilder());
        }

        [Fact]
        public async Task GetAccounts_WhenInvoked_ShouldReturnTheListOfAccount()
        {
            // Given
            IList<Account> accounts = _fixture.CreateMany<Account>().ToList();

            var accountServiceMock = _fixture.Freeze<Mock<IAccountService>>();
            accountServiceMock.Setup(x => x.GetAccounts()).Returns(Task.FromResult(accounts));

            var subject = _fixture.Create<AccountController>();

            // When
            var result = await subject.GetAccounts();

            // Then
            AccountAssert.Assert(accounts, result);
        }
        
        [Fact]
        public async Task GetAccount_WhenInvoked_ShouldReturnTheAccount()
        {
            // Given
            var account = _fixture.Create<Account>();

            var accountServiceMock = _fixture.Freeze<Mock<IAccountService>>();
            accountServiceMock.Setup(x => x.GetAccount(account.Id)).Returns(Task.FromResult(account));

            var subject = _fixture.Create<AccountController>();

            // When
            var result = await subject.GetAccount(account.Id);

            // Then
            AccountAssert.Assert(account, result);
        }
        
        [Fact]
        public async Task GetTransactions_WhenInvoked_ShouldReturnTheListOfTransaction()
        {
            // Given
            var accountId = _fixture.Create<string>();
            IList<Transaction> transactions = _fixture.For<Transaction>()
                .With(x => x.AccountId, accountId)
                .CreateMany()
                .ToList();

            var accountServiceMock = _fixture.Freeze<Mock<IAccountService>>();
            accountServiceMock.Setup(x => x.GetTransactions(accountId))
                .Returns(Task.FromResult(transactions));

            var subject = _fixture.Create<AccountController>();

            // When
            var result = await subject.GetTransactions(accountId);

            // Then
            TransactionAssert.Assert(transactions, result);
        }
    }
}
